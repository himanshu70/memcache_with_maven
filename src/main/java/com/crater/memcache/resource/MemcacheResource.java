package com.crater.memcache.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.crater.memcache.model.User;


@Path("/api/v1/users")
public interface MemcacheResource {

	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	Response register( User user);
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	Response getUser(@QueryParam("name") String name);
	
	@POST
	@Path("/name")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	Response getName(User user);
}
