package com.crater.memcache.resource.impl;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.crater.memcache.model.User;
import com.crater.memcache.resource.MemcacheResource;
import com.crater.memcache.service.MemcacheService;


@Component
public class MemcacheResourceImpl implements MemcacheResource
{

	private final MemcacheService memcacheService;
	
	@Autowired
	public MemcacheResourceImpl(final MemcacheService memcacheService) {
		this.memcacheService=memcacheService;
	}
	@Override
	public Response register(User user) {
			final User userDetail =  memcacheService.save(user);
			return Response.ok(userDetail).build();
	}
	@Override
	public Response getUser(String name) {
		return Response.ok( memcacheService.getUserByUserId(name)).build();
	}
	@Override
	public Response getName(User user) {
		
		return Response.ok( memcacheService.getName(user)).build();
	}
}
