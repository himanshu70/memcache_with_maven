package com.crater.memcache.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crater.memcache.dao.MemcacheDao;
import com.crater.memcache.dao.ProfileDao;
import com.crater.memcache.model.User;
import com.crater.memcache.service.MemcacheService;

@Service
public class MemcacheServiceImpl implements MemcacheService {

private final MemcacheDao memcacheDao;
private final ProfileDao profileDao;
	
	@Autowired
	public MemcacheServiceImpl(final MemcacheDao memcacheDao,final ProfileDao profileDao) {
		this.memcacheDao=memcacheDao;
		this.profileDao=profileDao;
	}
	
	@Override
	public User save(User user) {
		return memcacheDao.save(user);
		}

	@Override
	public User getUserByUserId(String name) {
		return memcacheDao.getUserByUserId(name);
	}

	@Override
	public Map<String, String> getName(User user) {
	String name= profileDao.getName(user);
	Map<String , String > h=new HashMap<>();
	h.put("name", name);
	return h;
	}

}
