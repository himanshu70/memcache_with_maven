package com.crater.memcache.service;

import java.util.Map;

import com.crater.memcache.model.User;

public interface MemcacheService {

	User save(User user);

	User getUserByUserId(String name);

	Map<String, String> getName(User user);

}
