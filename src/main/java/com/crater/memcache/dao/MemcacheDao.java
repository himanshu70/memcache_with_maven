package com.crater.memcache.dao;

import com.crater.memcache.model.User;

public interface MemcacheDao {

	User save(User user);

	User getUserByUserId(String name);

}
