package com.crater.memcache.dao;

import com.crater.memcache.model.User;

public interface ProfileDao {

	
	public String getName(User user);
}
