package com.crater.memcache.dao.impl;

import static com.crater.memcache.dao.impl.DefaultUtils.defaultStringIfNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.crater.memcache.dao.BaseDao;
import com.crater.memcache.dao.MemcacheDao;
import com.crater.memcache.model.User;


@EnableCaching
@Repository
public class MemcacheDaoImpl implements MemcacheDao{

	private final BaseDao baseDao;
	
	
	private static final String[] USER_ID_COLUMN_NAME = { "id" };
	
	@Autowired
	public MemcacheDaoImpl(final BaseDaoFactory baseDaoFactory) {
		this.baseDao = baseDaoFactory.createBaseDao("users.xml");
	}
	
	@CachePut(value = "defaultCache",key = "#user.getName()")
	@Override
	public User save(User user) {
		final String query = baseDao.getQueryById("save");
		final KeyHolder keyHolder = new GeneratedKeyHolder();
		final SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
				.addValue("emailId", defaultStringIfNull(user.getEmailId()))
				.addValue("name", defaultStringIfNull(user.getName()))
				.addValue("lastName", user.getLastName()) 
				.addValue("imageUrl", defaultStringIfNull(user.getImageUrl()))
				.addValue("referralCode", user.getReferralCode())
				.addValue("userType", 1)
				.addValue("cc", user.getCc())
				.addValue("phone", user.getPhone())
				.addValue("password", defaultStringIfNull(user.getPassword()))
				.addValue("referredById", user.getReferredById());
				baseDao.getJdbcTemplate().update(query, sqlParameterSource, keyHolder, USER_ID_COLUMN_NAME);
		return user;
	}

	
	@Cacheable(value = "defaultCache", key ="#name")
	@Override
	public User getUserByUserId(String name) {
		User user=new User(12L,"ab@gmail.com","himanshu","sharma",null,null,null,null,null,null,null,null);
		return user;
		
	}
	
}
