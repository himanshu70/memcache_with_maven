package com.crater.memcache.dao.impl;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.stereotype.Repository;

import com.crater.memcache.dao.ProfileDao;
import com.crater.memcache.model.User;

@EnableCaching
@Repository
@CacheConfig(cacheNames = { "defaultCache" })
public class ProfileDaoImpl implements ProfileDao {

	/*
	 * @Cacheable(condition="#user.getName()!='himanshu'" , key="#user.getName()")
	 *  public String getName(User user){ User us=new
	 * User(11L
	 * ,"abc@gmail.com","abc",null,null,null,null,null,null,null,null,null);
	 * return user.getName();
	 * 
	 * }
	 */

	@Cacheable(key = "#user.getName()", condition="#user.getCc().length()>1",unless = "#result == null")
	public String getName(User user) {
		User us = new User(11L, "abc@gmail.com", "abc", null, null, null, null,
				null, null, null, null, null);
		
		//return null;   -- if null uncommented unless wil not let them to b cached 
		//i.e.  unless works at second phase such that first check key if key not found -> then unless -> if 
		// unless condition satisfies cached or discards  
	    /*    >  if found return the result from cache
	    	  >  if not found evaluates method that return null
	    	  >  return value is matched with unless condition (which always return true)
	    	  >  null value do not get cached
	    	  >  null value is returned to caller*/
		
		return user.getName();
	}

}
