package com.crater.memcache.dao.impl;

import java.util.Collections;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;

import com.crater.memcache.dao.BaseDao;


/**
 * 
 * @author Navrattan Yadav
 *
 */
public class BaseDaoImpl implements BaseDao {

	private final NamedParameterJdbcOperations jdbcTemplate;
	private final Map<String, String> queryMap;
	
	
	public BaseDaoImpl(final NamedParameterJdbcOperations jdbcTemplate,
			final Map<String, String> queryMap) {
		this.jdbcTemplate = jdbcTemplate;
		this.queryMap = Collections.unmodifiableMap(queryMap);
		
	}

	@Override
	public NamedParameterJdbcOperations getJdbcTemplate() {
		return jdbcTemplate;
	}

	@Override
	public String getQueryById(final String id) {
		return queryMap.get(id);
	}

}
